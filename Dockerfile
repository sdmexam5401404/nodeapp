FROM node
WORKDIR /src
COPY . .
EXPOSE 2000
CMD node server.js

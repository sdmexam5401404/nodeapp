const express = require('express')
const app = express()

app.get("/", (req,res) =>
{
    res.send("Welcome to Sunbeam PGDAC")
})

app.listen(2000, () =>
{
    console.log("Server started on port no 2000")
})